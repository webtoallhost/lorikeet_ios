//
//  FilterByViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 07/03/21.
//

import UIKit

class FilterByViewController: UIViewController {

    @IBOutlet weak var mainIndicatorView: UIView!
    
    @IBOutlet weak var mainFilterByTableView: UITableView!
   
    @IBOutlet weak var subFilterTableView: UITableView!
  
    @IBOutlet weak var subIndicatorView: UIView!
    
    @IBOutlet weak var heightForSubTableView: NSLayoutConstraint!
  
    var mainFilterArr = ["sort By","Price","New Arrivals","Colors","Size","Brand","Location","Product","Discount","Available","Sales"]
    var subFilterArr = ["Relevence","Popularity","Price Low to High","Price Hight to Low"]
    var selectedMainIndex = 0
    var selectedSubIndex = 0

    
    override func viewDidLayoutSubviews() {
      self.subIndicatorView.roundCorners(corners: [.bottomRight, .topRight], radius: 10)
      self.mainIndicatorView.roundCorners(corners: [.bottomRight, .topRight], radius: 10)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainFilterByTableView.delegate = self
        subFilterTableView.delegate = self
        mainFilterByTableView.dataSource = self
        subFilterTableView.dataSource = self
        DispatchQueue.main.async {
           
        }
        

        mainFilterByTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        subFilterTableView.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")

        // Do any additional setup after loading the view.
    }
    



}
extension FilterByViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == mainFilterByTableView {
            return mainFilterArr.count
        }else{
            return subFilterArr.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == mainFilterByTableView {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as! FilterTableViewCell
            cell.FilterContentLabel.text = mainFilterArr[indexPath.row]
            if selectedMainIndex == indexPath.row {
                cell.FilterContentLabel.textColor = UIColor(named: "BlueColor")
            }else{
                cell.FilterContentLabel.textColor = UIColor(named: "LightGrayColor")
            }
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell", for: indexPath) as!
                FilterTableViewCell
            
            DispatchQueue.main.async {
                self.heightForSubTableView.constant = self.subFilterTableView.contentSize.height
            }
            if selectedSubIndex == indexPath.row {
                cell.FilterContentLabel.textColor = UIColor(named: "BlueColor")
            }else{
                cell.FilterContentLabel.textColor = UIColor(named: "LightGrayColor")
            }

            cell.FilterContentLabel.text = subFilterArr[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == mainFilterByTableView {
            let rectOfCell = tableView.rectForRow(at: indexPath)
            let rectOfCellInSuperview = tableView.convert(rectOfCell, to: tableView.superview)
            
            UIView.animate(withDuration: 0.1,
                       delay: 0.1,
                       options: UIView.AnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.mainIndicatorView.frame.origin.y = rectOfCellInSuperview.origin.y + 4
            }, completion: { (finished) -> Void in
            
            })
            print(rectOfCellInSuperview)
            selectedMainIndex = indexPath.row
            self.mainFilterByTableView.reloadData()
        }else{
            let rectOfCell = tableView.rectForRow(at: indexPath)
            let rectOfCellInSuperview = tableView.convert(rectOfCell, to: tableView.superview)
            self.subIndicatorView.frame.origin.y = rectOfCellInSuperview.origin.y + 4
            selectedSubIndex = indexPath.row
            self.subFilterTableView.reloadData()
            print(rectOfCellInSuperview)
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
}
