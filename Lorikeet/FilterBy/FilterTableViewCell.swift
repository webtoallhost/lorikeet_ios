//
//  FilterTableViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 07/03/21.
//

import UIKit

class FilterTableViewCell: UITableViewCell {

    @IBOutlet weak var FilterContentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
