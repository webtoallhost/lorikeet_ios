//
//  FabricDetailsCollectionViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/04/21.
//

import UIKit

class FabricDetailsCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var fabricDetailImageView: UIImageView!
    
}
