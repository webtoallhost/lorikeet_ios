//
//  TailorMenStyleHeaderCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/04/21.
//

import UIKit

class TailorMenStyleHeaderCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var sizeCollectionView: UICollectionView!
   
    @IBOutlet weak var heightForSizeCollectionView: NSLayoutConstraint!
    
    
    var x = 0
    var section = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sizeCollectionView.delegate = self
        sizeCollectionView.dataSource = self
        self.sizeCollectionView.register(UINib(nibName: "CommonImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonImageCollectionViewCell")
        
    }
    
    func reloadData(){
        self.sizeCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 2{
        return 6
        }else{
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonImageCollectionViewCell", for: indexPath) as! CommonImageCollectionViewCell
        cell.commonImage.image = UIImage(named: "sleeve_1")
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        if section == indexPath.row {
            return CGSize(width: (collectionView.bounds.width / 3)-20 ,height: heightForSizeCollectionView.constant)
        }else if section == indexPath.row{
            return CGSize(width: (collectionView.bounds.width / 3)-10 ,height: (heightForSizeCollectionView.constant/2)-20)
        }else{
            return CGSize(width: (collectionView.bounds.width / 3)-20 ,height: heightForSizeCollectionView.constant)
        }
    }
    
    func cellRefresh(){
        self.sizeCollectionView.reloadData()
    }
}
