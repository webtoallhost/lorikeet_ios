//
//  TailorListViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 05/04/21.
//

import UIKit

class TailorListViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var tailorListCollectionView: UICollectionView!
    
  
    var arr = ["tailor_model","tailor_model","tailor_model"]
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tailorListCollectionView.delegate = self
        tailorListCollectionView.dataSource = self
        self.tailorListCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
        DispatchQueue.main.async{
            self.tailorListCollectionView.reloadData()
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cell.modelimageView.image = UIImage(named: arr[indexPath.row])
        
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width / 2)-10 ,height:(collectionView.bounds.width / 2)+80)
    }
    
    
    @IBAction func onClickAdd(_ sender: Any) {
        self.pushViewController(name:"TailorMensFabricViewController")
    }
    
    
}
