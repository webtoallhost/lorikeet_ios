//
//  UploadPhotoListViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/04/21.
//

import UIKit
var arr = ["model1","model2","model3"]
class UploadPhotoListViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var photoListCollectionView: UICollectionView!
    
    @IBOutlet weak var heightForPhotoList: NSLayoutConstraint!
    
  
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        photoListCollectionView.delegate = self
        photoListCollectionView.dataSource = self
        self.photoListCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
        DispatchQueue.main.async{
            self.photoListCollectionView.reloadData()
        }
    }
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        DispatchQueue.main.async {
            self.heightForPhotoList.constant = self.photoListCollectionView.contentSize.height
        }
        cell.modelimageView.image = UIImage(named: arr[indexPath.row])
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width / 2)-10 ,height:(collectionView.bounds.width / 2)+80)
    }
    
    
    @IBAction func onClickRequestProfessionals(_ sender: Any) {
        self.pushViewController(name:"PhotoDetailsViewController")
    }
    
    
}
