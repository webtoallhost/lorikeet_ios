//
//  TailorMensStyleViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/04/21.
//

import UIKit

class TailorMensStyleViewController: UIViewController {
    
    
    @IBOutlet weak var mensStyleTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mensStyleTableView.delegate = self
        mensStyleTableView.dataSource = self
        self.mensStyleTableView.register(UINib(nibName: "TailorMenStyleHeaderCell", bundle: nil), forCellReuseIdentifier: "TailorMenStyleHeaderCell")
    }
    
}

extension TailorMensStyleViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TailorMenStyleHeaderCell", for: indexPath) as! TailorMenStyleHeaderCell
        
        DispatchQueue.main.async {
          //  self.heightForTableview.constant = self.showRoomListTableView.contentSize.height
        }
        cell.section = indexPath.row
        cell.heightForSizeCollectionView.constant = (self.view.bounds.width/3)+50
        cell.layoutIfNeeded()
        
        cell.cellRefresh()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
