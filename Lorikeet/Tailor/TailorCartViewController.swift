//
//  TailorCartViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/04/21.
//

import UIKit
var TitleArr = ["Color","Fabrics","Sleeve","Bottom","Collar"]
var imageArr = ["sleeve_1","sleeve_1","sleeve_1","sleeve_1","sleeve_1"]

class TailorCartViewController: UIViewController {
    
    @IBOutlet weak var heightForFabricDetails: NSLayoutConstraint!
    
    
    @IBOutlet weak var fabricDetailsCollectionView: UICollectionView!
    
    @IBOutlet weak var heightForPhotoList: NSLayoutConstraint!
    
    @IBOutlet weak var uploadPhotosCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fabricDetailsCollectionView.delegate = self
        fabricDetailsCollectionView.dataSource = self
        uploadPhotosCollectionView.delegate = self
        uploadPhotosCollectionView.dataSource = self
        self.uploadPhotosCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
    }
    @IBAction func onClickCheckOut(_ sender: Any) {
        self.pushViewController(name:"OrderSummaryViewController")
    }
    
}
extension TailorCartViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TitleArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == fabricDetailsCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FabricDetailsCollectionViewCell", for: indexPath) as! FabricDetailsCollectionViewCell
            DispatchQueue.main.async {
                self.heightForFabricDetails.constant = self.fabricDetailsCollectionView.contentSize.height
                cell.titleLabel.text = TitleArr[indexPath.row]
                cell.fabricDetailImageView.image = UIImage(named: imageArr[indexPath.row])
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            DispatchQueue.main.async {
                self.heightForPhotoList.constant = self.uploadPhotosCollectionView.contentSize.height
            }
            cell.modelimageView.image = UIImage(named: "model1")
            return cell
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        if collectionView == fabricDetailsCollectionView{
            return CGSize(width: 80 ,height: 70)
        }else{
            return CGSize(width: (collectionView.bounds.width / 2)-10 ,height:(collectionView.bounds.width / 2)+80)
        }
    }
    
    
}
