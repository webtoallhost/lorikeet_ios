//
//  FabricPhotoDetailsViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/04/21.
//

import UIKit

class FabricPhotoDetailsViewController: UIViewController {

    @IBOutlet weak var sizesCollectionView: UICollectionView!
    
    @IBOutlet weak var heightForSizeCollectionview: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        sizesCollectionView.delegate = self
        sizesCollectionView.dataSource = self
        
        self.sizesCollectionView.register(UINib(nibName:"HeaderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeaderCollectionViewCell")
    }
    
    @IBAction func onClickCheckOut(_ sender: Any) {
        self.pushViewController(name:"TailorCartViewController")
    }

}

extension FabricPhotoDetailsViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == sizesCollectionView{
            return sizeArr.count
        }
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == sizesCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCollectionViewCell", for: indexPath) as! HeaderCollectionViewCell
            cell.count = sizeArr[indexPath.row]
            cell.sizeCollectionView.tag = 0
            if indexPath.row == 1 {
                cell.descriptionLabel.text = "Solid Color"
            }else{
             cell.descriptionLabel.text = "Pattern"
            }
            cell.layoutIfNeeded()
            cell.reloadData()
            DispatchQueue.main.async {
                self.heightForSizeCollectionview.constant = self.sizesCollectionView.contentSize.height
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
            return CGSize(width: (25*sizeArr[indexPath.row])+20 ,height: 70)
    }
}
