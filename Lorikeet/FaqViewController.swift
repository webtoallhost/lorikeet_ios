//
//  FaqViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 02/04/21.
//

import UIKit

class FaqViewController: UIViewController {
    
    @IBOutlet weak var faqTableView: UITableView!
    
    var selectedIndex = [Int]()
    var singleIndex:Int?
    var settingsArr = ["Push Notification","Account Settings","Change Language","Live Chat","Need Help"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        faqTableView.delegate = self
        faqTableView.dataSource = self
        for i in 0..<settingsArr.count{
            selectedIndex.append(0)
        }
    }
}

extension FaqViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return settingsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaqTableViewCell", for: indexPath) as! FaqTableViewCell
        cell.downArrowBtn.tag = indexPath.row
        cell.downArrowBtn.addTarget(self, action: #selector(downArrowTapped), for: .touchUpInside)
        
        if singleIndex == indexPath.row{
            cell.downArrowImage.transform = cell.downArrowImage.transform.rotated(by: CGFloat(Double.pi))
        }
        
        if selectedIndex[indexPath.row] == 0{
            cell.magView.isHidden = true
        }else{
            cell.magView.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func downArrowTapped(_ sender:UIButton){
        singleIndex = sender.tag
        if selectedIndex[sender.tag] == 0{
            selectedIndex[sender.tag] = 1
        }else{
            selectedIndex[sender.tag] = 0
        }
        faqTableView.reloadData()
    }
}

