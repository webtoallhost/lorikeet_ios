//
//  ProfileViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/04/21.
//

import UIKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickEdit(_ sender: Any) {
        self.pushViewController(name: "EditProfileViewController")
    }
    
    @IBAction func OnClickAddress(_ sender: Any) {
        self.pushViewController(name: "AddAddressViewController")
    }
    
    @IBAction func onClickMeasurements(_ sender: Any) {
        self.pushViewController(name: "SelectYourSizeViewController")
    }
    
}
