//
//  FavouriteListViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 11/03/21.
//

import UIKit

class FavouriteListViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var favouriteListCollectionView: UICollectionView!
    
  
    var arr = ["model1","model2","model3"]
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        favouriteListCollectionView.delegate = self
        favouriteListCollectionView.dataSource = self
        self.favouriteListCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
        DispatchQueue.main.async{
            self.favouriteListCollectionView.reloadData()
        }
    }
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cell.modelimageView.image = UIImage(named: arr[indexPath.row])
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        
        return CGSize(width: (collectionView.bounds.width / 2)-10 ,height:(collectionView.bounds.width / 2)+80)
    }
    
    
    
    
}
