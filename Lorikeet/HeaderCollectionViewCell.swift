//
//  HeaderCollectionViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 05/04/21.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var sizeCollectionView: UICollectionView!
  
    var count = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sizeCollectionView.delegate = self
        sizeCollectionView.dataSource = self
        self.sizeCollectionView.register(UINib(nibName: "CommonViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonViewCollectionViewCell")
        
    }
    
    func reloadData(){
        self.sizeCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonViewCollectionViewCell", for: indexPath) as! CommonViewCollectionViewCell
        if collectionView.tag == 1{
          cell.sizeLabel.text = sizes[indexPath.row]
          cell.commonView.backgroundColor = .white
        }else if collectionView.tag == 0{
            cell.commonView.backgroundColor = color[indexPath.row]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 22, height: 22)
    }
}
