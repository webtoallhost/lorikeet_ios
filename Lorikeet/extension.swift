//
//  extension.swift
//  Lorikeet
//
//  Created by Elangovan  on 01/03/21.
//

import Foundation
import UIKit
import SideMenu

extension BinaryInteger {
    var degreesToRadians: CGFloat { CGFloat(self) * .pi / 180 }
}

extension FloatingPoint {
    var degreesToRadians: Self { self * .pi / 180 }
    var radiansToDegrees: Self { self * 180 / .pi }
}
extension UIView{
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    
     /// Set the view layer as an hexagon
     func setupHexagonView() {
         let lineWidth: CGFloat = 5
        let path = self.roundedPolygonPath(rect: self.bounds, lineWidth: lineWidth, sides: 6, cornerRadius: 6, rotationOffset: CGFloat(3.14))
         
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         mask.lineWidth = lineWidth
         mask.strokeColor = UIColor.clear.cgColor
         mask.fillColor = UIColor.white.cgColor
         self.layer.mask = mask
         
         let border = CAShapeLayer()
         border.path = path.cgPath
         border.lineWidth = lineWidth
         border.strokeColor = UIColor.white.cgColor
         border.fillColor = UIColor.clear.cgColor
         self.layer.addSublayer(border)
     }
    
    func drawOffCircle(start:CGFloat,end:CGFloat,clockState:Bool) {
       

       let path = UIBezierPath(arcCenter: CGPoint(x: self.frame.size.width/2, y: self.frame.size.height/2),
                            radius: self.frame.size.height/2,
                            startAngle: start.degreesToRadians,
                            endAngle: end.degreesToRadians,
                            clockwise: clockState)
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        layer.masksToBounds = true

        
    }
    
    func dashedView() {
        let  path = UIBezierPath()
        
        let  p0 = CGPoint(x: self.bounds.minX, y: self.bounds.midY)
        path.move(to: p0)

        let  p1 = CGPoint(x: self.bounds.maxX, y: self.bounds.midY)
        path.addLine(to: p1)

        let  dashes: [ CGFloat ] = [ 16.0, 32.0 ]
        path.setLineDash(dashes, count: dashes.count, phase: 0.0)

        path.lineWidth = 8.0
        path.lineCapStyle = .butt
        UIColor.blue.set()
        path.stroke()
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
     /// Makes a bezier path which can be used for a rounded polygon
     /// layer
     ///
     /// - Parameters:
     ///   - rect: uiview rect bounds
     ///   - lineWidth: width border line
     ///   - sides: number of polygon's sides
     ///   - cornerRadius: radius for corners
     ///   - rotationOffset: offset of rotation of the view
     /// - Returns: the newly created bezier path for layer mask
     public func roundedPolygonPath(rect: CGRect, lineWidth: CGFloat, sides: NSInteger, cornerRadius: CGFloat, rotationOffset: CGFloat = 0) -> UIBezierPath {
         let path = UIBezierPath()
         let theta: CGFloat = CGFloat(2.0 * .pi) / CGFloat(sides) // How much to turn at every corner
         let width = min(rect.size.width, rect.size.height)        // Width of the square
         
         let center = CGPoint(x: rect.origin.x + width / 2.0, y: rect.origin.y + width / 2.0)
         
         // Radius of the circle that encircles the polygon
         // Notice that the radius is adjusted for the corners, that way the largest outer
         // dimension of the resulting shape is always exactly the width - linewidth
         let radius = (width - lineWidth + cornerRadius - (cos(theta) * cornerRadius)) / 2.0
         
         // Start drawing at a point, which by default is at the right hand edge
         // but can be offset
         var angle = CGFloat(rotationOffset)
         
         let corner = CGPoint(x: center.x + (radius - cornerRadius) * cos(angle), y: center.y + (radius - cornerRadius) * sin(angle))
         path.move(to: CGPoint(x: corner.x + cornerRadius * cos(angle + theta), y: corner.y + cornerRadius * sin(angle + theta)))
         
         for _ in 0..<sides {
             angle += theta
             
             let corner = CGPoint(x: center.x + (radius - cornerRadius) * cos(angle), y: center.y + (radius - cornerRadius) * sin(angle))
             let tip = CGPoint(x: center.x + radius * cos(angle), y: center.y + radius * sin(angle))
             let start = CGPoint(x: corner.x + cornerRadius * cos(angle - theta), y: corner.y + cornerRadius * sin(angle - theta))
             let end = CGPoint(x: corner.x + cornerRadius * cos(angle + theta), y: corner.y + cornerRadius * sin(angle + theta))
             
             path.addLine(to: start)
             path.addQuadCurve(to: end, controlPoint: tip)
         }
         
         path.close()
         
         // Move the path to the correct origins
         let bounds = path.bounds
         let transform = CGAffineTransform(translationX: -bounds.origin.x + rect.origin.x + lineWidth / 2.0, y: -bounds.origin.y + rect.origin.y + lineWidth / 2.0)
         path.apply(transform)
         
         return path
     }
   
}

extension UIViewController{
 
 @IBAction func onClickMenu(_ sender: Any) {
        let vc = StoryBoard.instantiateViewController(withIdentifier: "SliderViewController") as! SliderViewController
        let menu = SideMenuNavigationController(rootViewController: vc)
    menu.leftSide = true
        present(menu, animated: true, completion: nil)
        
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
       
    @IBAction func onClickClear(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)

    }
    
    @IBAction func onClickFilter(_ sender: Any) {
        let vc = StoryBoard.instantiateViewController(withIdentifier: "FilterByViewController") as! FilterByViewController
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func onClickCart(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CartListViewController") as! CartListViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClickNotification(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotifiacationViewController") as! NotifiacationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushViewController(name:String){
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: name) else { return  }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
