//
//  CartLsitViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 10/03/21.
//

import UIKit

class CartListViewController: UIViewController {

    
    @IBOutlet weak var cartListTableView: UITableView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cartListTableView.delegate = self
        cartListTableView.dataSource = self
        
        self.cartListTableView.register(UINib(nibName: "CartTableViewCell", bundle: nil), forCellReuseIdentifier: "CartTableViewCell")
        
        // Do any additional setup after loading the view.
    }
   
    @IBAction func onClickSelectAddress(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressViewController") as! AddAddressViewController
        self.navigationController?.pushViewController(vc, animated: true)
    
        
    }
     //
}

extension CartListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
