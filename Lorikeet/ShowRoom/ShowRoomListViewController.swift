//
//  ShowRoomListViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 06/03/21.
//

import UIKit

class ShowRoomListViewController: UIViewController {

    
    @IBOutlet weak var categoryCollectionView: UICollectionView!

    @IBOutlet weak var showRoomListTableView: UITableView!
    
    @IBOutlet weak var heightForTableview: NSLayoutConstraint!
  
    @IBOutlet weak var showRoomCollectionsCollectionView: UICollectionView!
   
    @IBOutlet weak var heightForCollectionView: NSLayoutConstraint!
   
    var arr = ["model1","model2","model3"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showRoomListTableView.delegate = self
        showRoomListTableView.dataSource = self
        self.showRoomListTableView.register(UINib(nibName: "ShowRoomListTableViewCell", bundle: nil), forCellReuseIdentifier: "ShowRoomListTableViewCell")
      
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        self.categoryCollectionView.register(UINib(nibName: "ShowRoomListCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShowRoomListCollectionViewCell")
       
        showRoomCollectionsCollectionView.delegate = self
        showRoomCollectionsCollectionView.dataSource = self
        self.showRoomCollectionsCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
     
    }

}



extension ShowRoomListViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryCollectionView{
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowRoomListCollectionViewCell", for: indexPath) as! ShowRoomListCollectionViewCell
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            cell.modelimageView.image = UIImage(named: arr[indexPath.row])
            DispatchQueue.main.async {
                self.heightForCollectionView.constant = self.showRoomCollectionsCollectionView.contentSize.height
            }
            return cell
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
      ) -> CGSize {
        if collectionView == categoryCollectionView {
            return CGSize(width: 80 ,height: 95)
        }else{
            return CGSize(width: (collectionView.bounds.width / 2)-10 ,height:(collectionView.bounds.width / 2)+80)
        }
      }
    
   
}

extension ShowRoomListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowRoomListTableViewCell", for: indexPath) as! ShowRoomListTableViewCell
        
        DispatchQueue.main.async {
            self.heightForTableview.constant = self.showRoomListTableView.contentSize.height
        }
        
        cell.heightFordressCollectionView.constant = (self.view.bounds.width/3)+76
        cell.layoutIfNeeded()
       
        cell.cellRefresh()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
