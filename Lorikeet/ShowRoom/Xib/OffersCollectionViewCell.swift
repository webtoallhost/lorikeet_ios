//
//  OffersCollectionViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 21/03/21.
//

import UIKit
import ScalingCarousel

class OffersCollectionViewCell: UICollectionViewCell{

    @IBOutlet weak var rightCircleView: UIView!
    
    @IBOutlet weak var leftCircleView: UIView!
  
    @IBOutlet weak var lineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rightCircleView.drawOffCircle(start: 90, end: 270, clockState: true)
        leftCircleView.drawOffCircle(start: 270, end: 90, clockState: true)
        lineView.dashedView()
    }
}
