//
//  CommonImageCollectionViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/03/21.
//

import UIKit

class CommonImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var commonImage: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
