//
//  ShowRoomListCollectionViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 06/03/21.
//

import UIKit

class ShowRoomListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var outerView: UIView!
 
    override func awakeFromNib() {
        super.awakeFromNib()
        outerView.setupHexagonView()
        // Initialization code
    }

}
