//
//  CommonViewCollectionViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/03/21.
//

import UIKit

class CommonViewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var commonView: UIView!
    
    @IBOutlet weak var sizeLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
