//
//  ShowRoomListTableViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 21/03/21.
//

import UIKit
import ScalingCarousel


class ShowRoomListTableViewCell: UITableViewCell {

    @IBOutlet weak var dressCollectionCollectionView: UICollectionView!
  
    
    @IBOutlet weak var offersListCollectionView: UICollectionView!
    

    @IBOutlet weak var heightFordressCollectionView: NSLayoutConstraint!
    
    @IBOutlet weak var heightForOffersCollectionView: NSLayoutConstraint!

    
    var arr = ["model1","model2","model3"]
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dressCollectionCollectionView.delegate = self
        dressCollectionCollectionView.dataSource = self
        self.dressCollectionCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        heightForOffersCollectionView.constant = 200
        offersListCollectionView.delegate = self
        offersListCollectionView.dataSource = self
        self.offersListCollectionView.register(UINib(nibName: "OffersCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "OffersCollectionViewCell")
        DispatchQueue.main.async{
            self.dressCollectionCollectionView.reloadData()
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension ShowRoomListTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == dressCollectionCollectionView {
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cell.modelimageView.image = UIImage(named: arr[indexPath.row])
        return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OffersCollectionViewCell", for: indexPath) as! OffersCollectionViewCell
            
            DispatchQueue.main.async {
                cell.setNeedsLayout()
                cell.layoutIfNeeded()
            }
            return cell
        }
    }
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        if collectionView == dressCollectionCollectionView {
            return CGSize(width: (collectionView.bounds.width / 3) ,height: heightFordressCollectionView.constant)
        }else{
          return CGSize(width: (collectionView.bounds.width)-100 ,height: collectionView.bounds.height)
        }
    }
    
    func cellRefresh(){
        self.dressCollectionCollectionView.reloadData()
    }
}
