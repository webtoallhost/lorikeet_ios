//
//  SizedCollectionViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/03/21.
//

import UIKit

class SizedCollectionViewCell: UICollectionViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var sizeCollectionView: UICollectionView!
  
  //  var sizesArr = []
    var count = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sizeCollectionView.delegate = self
        sizeCollectionView.dataSource = self
        self.sizeCollectionView.register(UINib(nibName: "CommonImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonImageCollectionViewCell")
        
    }
    
    func reloadData(){
        self.sizeCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonImageCollectionViewCell", for: indexPath) as! CommonImageCollectionViewCell
       /// cell.
       // cell.commonImage.image =
       
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width / 3), height: 100)
    }
}
