//
//  ShowRoomDetailsViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 08/03/21.
//

import UIKit
var sizeArr = [10,4,11]
var sizes = ["S","M","L","XL"]
var color = [UIColor.lightGray,UIColor.green,UIColor.blue,UIColor.red,UIColor.yellow,UIColor.green,UIColor.cyan,UIColor.green,UIColor.systemBlue,UIColor.black,UIColor.white]

class ShowRoomDetailsViewController: UIViewController {
    
    
    @IBOutlet weak var leftCornerView: UIView!
    
    @IBOutlet weak var heightForSizeCollectionview: NSLayoutConstraint!
    
    @IBOutlet weak var heightForRealatedTableview: NSLayoutConstraint!
    
    @IBOutlet weak var fabricVisualCollectionView: UICollectionView!
    
    @IBOutlet weak var sizesCollectionView: UICollectionView!
    
    @IBOutlet weak var relatedTableView: UITableView!
    var modelimageArr = ["slide_show1","slide_show1","slide_show1"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fabricVisualCollectionView.delegate = self
        fabricVisualCollectionView.dataSource = self
        
        self.fabricVisualCollectionView.register(UINib(nibName: "CommonImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonImageCollectionViewCell")
        
        sizesCollectionView.delegate = self
        sizesCollectionView.dataSource = self
        
        self.sizesCollectionView.register(UINib(nibName:"HeaderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeaderCollectionViewCell")
       
        
        relatedTableView.delegate = self
        relatedTableView.dataSource = self
        
        self.relatedTableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
    }
   
    override func viewDidLayoutSubviews() {
       // leftCornerView.roundCorners(corners: [.topLeft], radius: 3)
    }
}

extension ShowRoomDetailsViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == sizesCollectionView{
            return sizeArr.count
        }else if collectionView == fabricVisualCollectionView {
            return modelimageArr.count
        }
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == sizesCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderCollectionViewCell", for: indexPath) as! SizedCollectionViewCell
            cell.count = sizeArr[indexPath.row]
            cell.sizeCollectionView.tag = indexPath.row
            if indexPath.row == 1 {
                cell.descriptionLabel.text = "Choose Size"
            }else{
             cell.descriptionLabel.text = "Choose Color"
            }
            cell.layoutIfNeeded()
            cell.reloadData()
            DispatchQueue.main.async {
                self.heightForSizeCollectionview.constant = self.sizesCollectionView.contentSize.height
            }
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonImageCollectionViewCell", for: indexPath) as! CommonImageCollectionViewCell
            cell.commonImage.image = UIImage(named: modelimageArr[indexPath.row])
            return cell
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        if collectionView == sizesCollectionView{
            return CGSize(width: (25*sizeArr[indexPath.row])+20 ,height: 70)
        }else{
            return CGSize(width: (collectionView.bounds.height/4) ,height: (collectionView.bounds.height/4)+10)
        }
    }
}
extension ShowRoomDetailsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
           DispatchQueue.main.async {
            self.heightForRealatedTableview.constant = self.relatedTableView.contentSize.height
           }
        cell.heightForCollectionView.constant = (self.view.bounds.width/3)+106
        cell.layoutIfNeeded()
        cell.cellRefresh()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
