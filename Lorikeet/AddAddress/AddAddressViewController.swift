//
//  AddAddressViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 10/03/21.
//

import UIKit

class AddAddressViewController: UIViewController {
    
    
    @IBOutlet weak var addressListTableView: UITableView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressListTableView.delegate = self
        addressListTableView.dataSource = self
        
        self.addressListTableView.register(UINib(nibName: "AddAddressTableViewCell", bundle: nil), forCellReuseIdentifier: "AddAddressTableViewCell")
        
        // Do any additional setup after loading the view.
    }
}
extension AddAddressViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddAddressTableViewCell", for: indexPath) as! AddAddressTableViewCell
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SelectDeliveryAddressViewController") as! SelectDeliveryAddressViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
