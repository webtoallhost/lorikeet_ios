//
//  SelectYourSizeViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 01/04/21.
//

import UIKit

class SelectYourSizeViewController: UIViewController {
    
    @IBOutlet weak var bustSizeCollectionView: UICollectionView!
    @IBOutlet weak var waistSizeCollectionView: UICollectionView!
    @IBOutlet weak var hipsSizeCollectionView: UICollectionView!
    @IBOutlet weak var sizeCollectionView: UICollectionView!
    var sizeArr = ["33''","34''","35''","36''","37''","38''"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bustSizeCollectionView.delegate = self
        bustSizeCollectionView.dataSource = self
        self.bustSizeCollectionView.register(UINib(nibName: "CommonViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonViewCollectionViewCell")
        
        waistSizeCollectionView.delegate = self
        waistSizeCollectionView.dataSource = self
        self.waistSizeCollectionView.register(UINib(nibName: "CommonViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonViewCollectionViewCell")
        
        hipsSizeCollectionView.delegate = self
        hipsSizeCollectionView.dataSource = self

        self.hipsSizeCollectionView.register(UINib(nibName: "CommonViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonViewCollectionViewCell")
        
        sizeCollectionView.delegate = self
        sizeCollectionView.dataSource = self
        self.sizeCollectionView.register(UINib(nibName: "CommonViewCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CommonViewCollectionViewCell")
    }

}

extension SelectYourSizeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sizeArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommonViewCollectionViewCell", for: indexPath) as! CommonViewCollectionViewCell
        cell.sizeLabel.text = sizeArr[indexPath.row]
            return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
      ) -> CGSize {
            return CGSize(width: collectionView.bounds.width ,height: 40)

      }
    
   
}
