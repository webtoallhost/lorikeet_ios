//
//  HomeTableViewCell.swift
//  Lorikeet
//
//  Created by Elangovan  on 04/03/21.
//

import UIKit
protocol cellSelected{
    func cellSelected()
}
class HomeTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
   @IBOutlet weak var HomeCollectionView: UICollectionView!

   @IBOutlet weak var heightForCollectionView: NSLayoutConstraint!
    
    
    var arr = ["model1","model2","model3"]
  
    var delegate:cellSelected?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        HomeCollectionView.delegate = self
        HomeCollectionView.dataSource = self
        self.HomeCollectionView.register(UINib(nibName: "HomeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        
        DispatchQueue.main.async{
            self.HomeCollectionView.reloadData()
        }
        // Initialization code
    }
    
    
    func cellRefresh(){
        self.HomeCollectionView.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
        cell.modelimageView.image = UIImage(named: arr[indexPath.row])
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {

        return CGSize(width: (collectionView.bounds.width / 3) ,height: (self.HomeCollectionView.bounds.width/3)+106)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.cellSelected()
    }
    
    
}
