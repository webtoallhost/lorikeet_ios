//
//  HomeViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 03/03/21.
//

import UIKit
import SideMenu

class HomeViewController: UIViewController,cellSelected{
    
    @IBOutlet weak var heightForTableview: NSLayoutConstraint!
    
    
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    
    @IBOutlet weak var homeTableView: UITableView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        self.bannerCollectionView.register(UINib(nibName: "BannerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionViewCell")
       
        homeTableView.delegate = self
        homeTableView.dataSource = self
        
        self.homeTableView.register(UINib(nibName: "HomeTableViewCell", bundle: nil), forCellReuseIdentifier: "HomeTableViewCell")
        
       let vc = StoryBoard.instantiateViewController(withIdentifier: "SliderViewController") as! SliderViewController
       
        
        let leftMenuNavigationController = SideMenuNavigationController(rootViewController: vc)
        SideMenuManager.default.rightMenuNavigationController = leftMenuNavigationController
       
       
        SideMenuManager.default.addPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)

        // (Optional) Prevent status bar area from turning black when menu appears:
        leftMenuNavigationController.statusBarEndAlpha = 0
        // Copy all settings to the other menu
       
    }
    
}

extension HomeViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
        return cell
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
      ) -> CGSize {
     
        return CGSize(width: (collectionView.bounds.width)-40 ,height: collectionView.bounds.height)
      }
    
    
}

extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
        
        DispatchQueue.main.async {
            self.heightForTableview.constant = self.homeTableView.contentSize.height
        }
        
        cell.heightForCollectionView.constant = (self.view.bounds.width/3)+106
        cell.layoutIfNeeded()
        cell.delegate = self
        cell.cellRefresh()
        return cell
    }
    
    func cellSelected(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ShowRoomDetailsViewController") as! ShowRoomDetailsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
