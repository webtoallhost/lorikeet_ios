//
//  SliderViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 21/03/21.
//

import UIKit

class SliderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

    
    @IBOutlet weak var heightForSliderTableview: NSLayoutConstraint!
    
    @IBOutlet weak var sliderTableView: UITableView!

    @IBOutlet weak var indicatorView: UIView!
    var sideMenuArr = ["Home","My Orders","Settings","Register as Store","Chats","Terms and conditions","Privaxcy Policy","About Us"]
    var selectedSubIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sliderTableView.delegate = self
        sliderTableView.dataSource = self
      
        self.navigationController?.navigationBar.isHidden = true
        self.sliderTableView.register(UINib(nibName: "SliderTableViewCell", bundle: nil), forCellReuseIdentifier: "SliderTableViewCell")
    }
    
    
    override func viewDidLayoutSubviews() {
      self.indicatorView.roundCorners(corners: [.bottomRight, .topRight], radius: 10)
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell", for: indexPath) as! SliderTableViewCell
        cell.titileLabel.text = sideMenuArr[indexPath.row]
        DispatchQueue.main.async {
            self.heightForSliderTableview.constant = self.sliderTableView.contentSize.height
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let rectOfCell = tableView.rectForRow(at: indexPath)
        let rectOfCellInSuperview = tableView.convert(rectOfCell, to: tableView.superview)
        self.indicatorView.frame.origin.y = rectOfCellInSuperview.origin.y + 4
        selectedSubIndex = indexPath.row
        self.sliderTableView.reloadData()
        if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyOrdersListViewController") as! MyOrdersListViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SetttingsViewController") as! SetttingsViewController
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
        
    }

}
