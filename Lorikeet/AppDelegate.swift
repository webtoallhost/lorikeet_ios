//
//  AppDelegate.swift
//  Lorikeet
//
//  Created by Elangovan  on 01/03/21.
//

import UIKit
let appDelegate = UIApplication.shared.delegate as? AppDelegate
let StoryBoard = UIStoryboard(name: "Main", bundle: nil)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    func goToHome(){
        let signInPage = StoryBoard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
        let appDelegate = UIApplication.shared.delegate
        appDelegate?.window??.rootViewController = signInPage
    }
}

