//
//  SelectDeliveryAddressViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 31/03/21.
//

import UIKit

class SelectDeliveryAddressViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickEnterManual(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterManualAddressViewController") as! EnterManualAddressViewController
        self.navigationController?.pushViewController(vc, animated: true)
    
        
    }

}
