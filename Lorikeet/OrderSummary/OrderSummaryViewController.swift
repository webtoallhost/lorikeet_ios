//
//  OrderSummaryViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 10/03/21.
//

import UIKit

class OrderSummaryViewController: UIViewController {
    
    
    @IBOutlet weak var heightForCardListTableview: NSLayoutConstraint!
    @IBOutlet weak var cardListTableView: UITableView!
    @IBOutlet weak var orderSummaryTableView: UITableView!
    @IBOutlet weak var heightFororderSummary: NSLayoutConstraint!
    @IBOutlet weak var addReviewView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cardListTableView.delegate = self
        cardListTableView.dataSource = self
        self.cardListTableView.register(UINib(nibName: "CardListTableViewCell", bundle: nil), forCellReuseIdentifier: "CardListTableViewCell")
        addReviewView.frame = UIScreen.main.bounds
        view.addSubview(addReviewView)
        addReviewView.isHidden = true
        orderSummaryTableView.delegate = self
        orderSummaryTableView.dataSource = self
    }
    
    @IBAction func onClickDropDownArrow(_ sender: Any) {
        if orderSummaryTableView.isHidden{
            orderSummaryTableView.isHidden = false
        }else{
            orderSummaryTableView.isHidden = true
        }
    }
    
    @IBAction func onClickcontinue(_ sender: Any) {
        addReviewView.isHidden = false
    }
    
    @IBAction func onClickTrackOrder(_ sender: Any) {
        self.pushViewController(name: "TrackOrderViewController")
    }
}
extension OrderSummaryViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == cardListTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CardListTableViewCell", for: indexPath) as! CardListTableViewCell
            
            DispatchQueue.main.async {
                self.heightForCardListTableview.constant = self.cardListTableView.contentSize.height
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderSummaryTableViewCell", for: indexPath) as! OrderSummaryTableViewCell
            
            DispatchQueue.main.async {
                self.heightForCardListTableview.constant = self.cardListTableView.contentSize.height
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
