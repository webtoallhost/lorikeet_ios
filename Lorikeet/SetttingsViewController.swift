//
//  SetttingsViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 02/04/21.
//

import UIKit

class SetttingsViewController: UIViewController {

   @IBOutlet weak var settingsTableView: UITableView!
   var settingsArr = ["Push Notification","Account Settings","Change Language","Live Chat","Need Help"]
    override func viewDidLoad() {
        super.viewDidLoad()
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
    
    }
}

extension SetttingsViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return settingsArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        cell.titleLabel.text = settingsArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PushNotificationViewController") as! PushNotificationViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
