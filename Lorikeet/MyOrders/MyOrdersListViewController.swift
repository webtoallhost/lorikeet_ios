//
//  MyOrdersListViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 11/03/21.
//

import UIKit

class MyOrdersListViewController: UIViewController {

    @IBOutlet weak var myOrdersListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myOrdersListTableView.delegate = self
        myOrdersListTableView.dataSource = self
        
        self.myOrdersListTableView.register(UINib(nibName: "MyOrdersListTableViewCell", bundle: nil), forCellReuseIdentifier: "MyOrdersListTableViewCell")
    }
    
    @IBAction func onClickCheckOut(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension MyOrdersListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrdersListTableViewCell", for: indexPath) as! MyOrdersListTableViewCell
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
