//
//  PushNotificationViewController.swift
//  Lorikeet
//
//  Created by Elangovan  on 02/04/21.
//

import UIKit

class PushNotificationViewController: UIViewController {

   @IBOutlet weak var pushNotificationTableView: UITableView!
 
    var settingsArr = ["Offers","Message from Admin","Order Notifications","Appointment"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pushNotificationTableView.delegate = self
        pushNotificationTableView.dataSource = self
    
    }
}

extension PushNotificationViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PushNotificationTableViewCell", for: indexPath) as! PushNotificationTableViewCell
        cell.titleLabel.text = settingsArr[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
